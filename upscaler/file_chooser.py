# file_chooser.py: file chooser dialogs for opening and outputting files
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

from os.path import basename
from gi.repository import Gtk
from upscaler.filters import get_format_filters, supported_filters, image_filters
from gettext import gettext as _


class FileChooser:

    """ Open and load file. """
    @staticmethod
    def open_file(parent, *args):
        def load_file(_dialog, response):

            """ Return if user cancels. """
            if response != Gtk.ResponseType.ACCEPT:
                return

            """ Run if the user selects an image. """
            parent.on_load_file(dialog.get_file())

        dialog = Gtk.FileChooserNative.new(
            title=_('Select an image'),
            parent=parent,
            action=Gtk.FileChooserAction.OPEN
        )
        dialog.set_modal(True)
        dialog.connect('response', load_file)
        dialog.add_filter(supported_filters())
        dialog.show()

    """ Select output location. """
    @staticmethod
    def output_file(parent, default_name, good, bad, *args):
        def upscale_content(_dialog, response):

            """ Set output file path if user selects a location. """
            if response != Gtk.ResponseType.ACCEPT:
                bad(None)
                return

            """ Get all filters. """
            filters = []
            for filter in get_format_filters('image'):
                filters.append(filter.split('/').pop())

            """ Check if output file has a file extension or format is supported. """
            if '.' not in basename(dialog.get_file().get_path()):
                bad(_('No file extension was specified'))
                return

            elif basename(dialog.get_file().get_path()).split('.').pop().lower() not in filters:
                filename = basename(dialog.get_file().get_path()).split('.').pop()
                bad(_(f'’{filename}’ is an unsupported format'))
                return

            output_file_path = dialog.get_file().get_path()
            print(f'Output file: {output_file_path}')
            good(output_file_path)

        dialog = Gtk.FileChooserNative.new(
            title=_('Select output location'),
            parent=parent,
            action=Gtk.FileChooserAction.SAVE
        )
        dialog.set_modal(True)
        dialog.connect('response', upscale_content)
        dialog.add_filter(image_filters())
        dialog.set_current_name(default_name)
        dialog.show()
