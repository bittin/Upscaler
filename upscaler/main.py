# main.py: main application
#
# Copyright (C) 2022 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

import sys
from gi.repository import Adw, Gtk, Gio, GLib
from upscaler.window import UpscalerWindow
from upscaler.app_profile import APP_ID,APP_NAME,APP_VERSION,APP_ICON_NAME
from gettext import gettext as _

class UpscalerApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id=APP_ID,
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE)
        self.create_action('quit', self.__quit, ['<primary>q'])
        self.create_action('about', self.__about_action)
        self.create_action('open', self.__open_file, ['<primary>o'])
        self.create_action('open-output', self.__open_output, param=GLib.VariantType('s'))
        self.create_action('new-window', self.__new_window, ['<primary>n'])
        self.file = None

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.props.active_window
        if not win:
            win = UpscalerWindow(application=self)
        win.present()
        if self.file is not None:
            print(self.file)
            win.on_load_file(Gio.File.new_for_path(self.file))

    def __open_file(self, *args):
        self.props.active_window.open_file()

    def __open_output(self, app, data):
        file_path = data.unpack()
        file = open(file_path, 'r')
        fid = file.fileno()
        connection = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        proxy = Gio.DBusProxy.new_sync(connection,
                                       Gio.DBusProxyFlags.NONE,
                                       None,
                                       'org.freedesktop.portal.Desktop',
                                       '/org/freedesktop/portal/desktop',
                                       'org.freedesktop.portal.OpenURI',
                                       None)

        try:
            proxy.call_with_unix_fd_list_sync('OpenFile',
                                              GLib.Variant('(sha{sv})', ('', 0, {'ask': GLib.Variant('b', True)})),
                                              Gio.DBusCallFlags.NONE,
                                              -1,
                                              Gio.UnixFDList.new_from_array([fid]),
                                              None)
        except Exception as e:
            print(f'Error: {e}')

    def __about_action(self, *args):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name=APP_NAME,
                                application_icon=APP_ICON_NAME,
                                developer_name='Upscaler Contributors',
                                version=APP_VERSION,
                                copyright='Copyright © 2022 Upscaler Contributors',
                                license_type=Gtk.License.GPL_3_0_ONLY,
                                website='https://gitlab.com/TheEvilSkeleton/Upscaler',
                                issue_url='https://gitlab.com/TheEvilSkeleton/Upscaler/-/issues',
                                support_url='https://matrix.to/#/#upscaler:matrix.org')
        about.set_translator_credits(translators())
        about.set_developers(developers())
        about.add_acknowledgement_section(
            _("Algorithms by"),
            [
                "Real-ESRGAN https://github.com/xinntao/Real-ESRGAN",
                "Real-ESRGAN ncnn Vulkan https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan",
            ]
        )
        about.add_acknowledgement_section(
            _("Code and Design Borrowed from"),
            [
                "Avvie https://github.com/Taiko2k/Avvie",
                "Bottles https://github.com/bottlesdevs/Bottles",
                "Loupe https://gitlab.gnome.org/BrainBlasted/loupe",
                "Totem https://gitlab.gnome.org/GNOME/totem",
                "Fractal https://gitlab.gnome.org/GNOME/fractal",
            ]
        )
        about.add_acknowledgement_section(
            _("Sample Image from"),
            [
                "Princess Hinghoi https://safebooru.org/index.php?page=post&s=view&id=3084434",
            ]
        )
        about.add_legal_section(
            title='Real-ESRGAN ncnn Vulkan',
            copyright='Copyright © 2021 Xintao Wang',
            license_type=Gtk.License.MIT_X11,
        )
        about.present()

    def create_action(self, name, callback, shortcuts=None, param=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
            param: an optional list of parameters for the action
        """
        action = Gio.SimpleAction.new(name, param)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)

    def do_command_line(self, command_line):
        args = command_line.get_arguments()
        if len(args) > 1:
            self.file = command_line.create_file_for_arg(args[1]).get_path()
        self.activate()
        return 0

    def __new_window(self, *args):
        win = UpscalerWindow(application=self)
        win.present()

    """ Quit application. """
    def __quit(self, _args, *args):
        win = self.props.active_window
        if win:
            win.destroy()

def translators():
    """ Translators list. To add yourself into the list, add '\n', followed by
        your name/username, and optionally an email or URL:

        Name only:    \nHari Rana
        Name + URL:   \nHari Rana https://theevilskeleton.gitlab.io
        Name + Email: \nHari Rana <theevilskeleton@riseup.net>
    """
    return _('Jürgen Benvenuti <gastornis@posteo.org>\nPhilip Goto <philip.goto@gmail.com>\nSabri Ünal <libreajans@gmail.com>\nyukidream https://fosstodon.org/@yukidream\nAnatoly Bogomolov <tolya.bogomolov2019@gmail.com>\nÅke Engelbrektson <eson@svenskasprakfiler.se>\nOnuralp SEZER <thunderbirdtr@fedoraproject.org>')

def developers():
    """ Developers/Contributors list. If you have contributed code, feel free
        to add yourself into the Python list:

        Name only:    \nHari Rana
        Name + URL:   \nHari Rana https://theevilskeleton.gitlab.io
        Name + Email: \nHari Rana <theevilskeleton@riseup.net>
    """
    return ['Hari Rana (TheEvilSkeleton) https://theevilskeleton.gitlab.io','Matteo', 'Onuralp SEZER <thunderbirdtr@fedoraproject.org>', 'Khaleel Al-Adhami <khaleel.aladhami@gmail.com>', 'gregorni https://gitlab.com/gregorni']

def main(version):
    """The application's entry point."""
    app = UpscalerApplication()
    return app.run(sys.argv)
